Sources for the personnel webpage of Amelie Aussel, researcher in team Mnemosyne of Inria Bordeaux - Sud-Ouest
---

# Contribution guide
The website is built using [Hugo](https://gohugo.io/).

It is organised into two main directories:
  - the `content` directory that contains the content of the
    website. Adding a file at one of the Hugo content formats (`md`,
    `org`...) in this directory or one of its subdirectories leads to
    add a new webpage to the site and a new post to the homepage of
    the website;
  - the `static` directory for files that get served statically on the
    site root. For example, it has to be used to store resources at
    Hugo content formats if you want to provide them for download but
    not to use them as website pages (for example for sources of the
    _Midis de la Bidouille_ at `.org` file format).

The website is automatically built and deployed by the continuous
integration at each modification of the `master` branch.

## Build the website locally
Hugo allow to build and check the website locally before public modifications:

  - 1. Install hugo: https://gohugo.io/installation/;
  - 2. Run the following command at the project root: `hugo server -D`;  
    (Remark: on OSX, it may be needed to replace the `.Hugo.Generator`
    keyword by the `hugo.Generator` one in the
    `themes/story/layouts/_default/baseof.html` file)
  - 3. Open the `http://localhost:<nnnn>` address in your web browser
    (where `<nnnn>` has to be relaced by the value provided by Hugo in
    your terminal).

## Bring your modifications

### Modify the text of the homepage
The text of the homepage can be edited in definition of the layout of the homepage: `themes/story/layouts/_default/baseof.html`

## Publish the new website
  - 1. Create a new branch, commit and push your modifications;
  - 2. If the continuous integration is Ok, creates a PR;
  - 3. Wait for a review or integrate your PR by yourself;
  - 4. The website will be published in few minutes.

## References
  - Hugo: https://gohugo.io/ (for a quick start: https://gohugo.io/getting-started/quick-start/)
