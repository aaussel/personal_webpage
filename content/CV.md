---
title: CV
url: "/cv"
image: "img/timescale.jpeg"
thumbnail: "img/timescale_500x500.jpeg"
color: black 
---

Here is my CV, in [PDF format](/content/img/CV_Amelie_Aussel.pdf), and in text below. 

**Amélie Aussel**

ISFP at Inria Bordeaux – Sud-Ouest, team Mnemosyne  
Date of birth: 02.06.1994, Nationality: French  
Email: amelie.aussel@inria.fr  
ORCID: https://orcid.org/0000-0003-0498-2905  
Personal webpage: https://sites.google.com/site/amelieausselresearch/  


# Past research experience

 | Subject | Date | Position and Laboratory | Supervisor |
|:--:|:--:|:--:|:--:|
| Computational modeling of thalamocortical interactions in visual attention | 2019-2021 | **Postdoc** in Boston University, Department of Mathematics and Statistics, Sylvio O. Conte Center of Neuroscience Research | Nancy Kopell |
| Computational modeling of healthy and epileptic hippocampal oscillations | 2016-2019 | **PhD student** in Loria and CRAN, Université de Lorraine, France | Radu Ranta and Laure Buhry |
| Stability Analysis of a dynamical neural network model of place cells: the “ring attractor” network | 2016 | **Master Student** in CRAN, Nancy, France | Radu Ranta |
| Development of a hand orthosis for stroke patients controlled by motor imagery | 2015 | Exchange semester in the Laboratory for Rehabilitation Neurosciences, Keio University, Japan | Junichi Ushiba |

----------------------------------------------------------

# Education

  *  2016 – 2019:  Teaching for higher education training at the Université de Lorraine. Obtention of the “Label Enseignement Superieur du Doctorat de l’Université de Lorraine”.
  *  2019 : Erasmus + formation at the Nencki Institute of Experimental Biology (Poland), on in-vivo animal neural recordings (with Daniel Wojcik and Mark Hunt).
  *  2017: Berkeley course in mining and modeling of neuroscience data, University of California Berkeley.
  *  2013-2016: Engineering degree at the Ecole Centrale de Lyon (specialization: BioEngineering and Nanotechnologies / Research, Innovation and Development Engineering) 
  *  2015-2016: Master degree in Engineering for Health and Medicine (specialization: Medical Imagery), at the Université Claude Bernard Lyon 1. 
  *  2011-2013: Classes préparatoires aux grandes écoles, with specialized teaching in Mathematics and Informatics, at lycée Fabert, Metz (France)

----------------------------------------------------------

# Teaching and scientific outreach activities

* 2023 – present: Lecturer for the Programming and Data Analysis for Neuroscience at the Bordeaux University International Master in Neuroscience
* 2022 – present: Lecturer for the Algorithmique de Graphes classes at the ENSEIRB Matmeca engineering school
* 2022 – present: Lecturer and Supervisor of usecase studies at the AI4Industry workshop.
* 2016 – 2019: Teaching Assistant for Python classes at the Ecole des Mines engineering school in Nancy.
* 2017- 2018: Lecturer for Computational Neuroscience classes for Master 2 students at the Université of Lorraine

I'm involved in many science outreach activities. Please check out the scientific outreach page
[here](https://aaussel.gitlabpages.inria.fr/personal_webpage/outreach/) for more details.

----------------------------------------------------------

# Publications

Please check out the pubilcations page
[here](https://aaussel.gitlabpages.inria.fr/personal_webpage/publications/)

See also my HAL publications page [here](https://haltools.archives-ouvertes.fr/Public/afficheRequetePubli.php?auteur_exp=Am%C3%A9lie%2CAussel&idHal=996411&CB_auteur=oui&CB_titre=oui&CB_article=oui&CB_DOI=oui&CB_Resume_court=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css)

----------------------------------------------------------

# Co-supervision of students
  * 2023: Mathilde Reynes, Master 2 student at the Université Côte d’Azur. “Reproduction of a A biologically realistic model of the human thalamo-cortical system”.
  * 2022 – present: Maeva Andriantsoamberomanga, PhD student at the University of Bordeaux. “Computational modeling of the hippocampus with detailed neuronal morphology for the interaction of neuronal oscillations with synaptic plasticity and electrical stimulation”.
  * 2020 – present (cosupervision since 2021): Nikolaos Vardalakis, PhD student at the University of Bordeaux. “Effects of neurostimulation of the hippocampal formation on neural oscillations: a computational exploration”.
----------------------------------------------------------

# Other commitments to the scientific community
  * 2023 – 2026: Program Committee member of the Organization for Computational Neurosciences (OCNS)
  * 2023 – present: member of the Conseil du Département Bordeaux Neurocampus
  * 2021 – present : member of the OCNS software working group
  * 2018- 2019: Elected member of the scientific council AM2I of the Université de Lorraine as PhD student representative
Reviewing activities (grant proposals and journals): ANR, PNAS




