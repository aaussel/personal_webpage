---
title: Outreach activities
url: /outreach

image: "img/outreach_banner.jpeg"
thumbnail: "img/outreach1-500x500.jpeg"
---


Alongside my research, I am doing a lot of science outreach activities. (page is still WIP)

<section class="spotlightstyle1">
    <div class="content">
        <u><b>December 2023:</b></u> Interview for the "Une minute avec..." program from Inria. You can view it on Youtube <a href="https://www.youtube.com/watch?v=uErBBgGxzwU ">here</a>.
    </div>
    <div class="image">
        <img src="../img/une_minute_avec.jpg" alt="Une minute avec..." />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>Février 2024:</b></u> Visit of the Collège Noes in Pessac with the Association Femmes et Sciences.
    </div>
    <div class="image">
        <img src="../img/Coupure_presse_college_Noes.png" alt="Article_presse_Noes" />
    </div>
</section>

----------------------------------------------------------

**Janvier 2024 - ongoing :** Mentoring of a PhD student as part of the Association Femmes et Sciences.
  
----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>December 2023:</b></u> Visit of the Lycée Les Iris in Lormont as part of the “1 Scientifique, 1 Classe, Chiche !” program.
    </div>
    <div class="image">
        <img src="../img/Logo_1-Scientifique_1-Classe_CHICHE-1.png" alt="LogoChiche2023" />
    </div>
</section>

----------------------------------------------------------

**November 2023:** Meeting with the students of the ENS Lyon engineering school visiting Bordeaux.
  
----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>October 2023:</b></u> Participation in Inria's Fête de la Science (Circuit Scientifique Bordelais), animation of the "Datagramme" boardgame about numerical sciences.
    </div>
    <div class="image">
        <img src="../img/logo_circuit_scientifique_bordelais.png" alt="CircuitBordelais2023" />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>March 2023:</b></u> Participation in a conference on AI and psychiatry in Civray for the "Semaine du Cerveau".
    </div>
    <div class="image">
        <img src="../img/Semaine_cerveau.png" alt="SemaineCerveau" />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>December 2022:</b></u> Visit of the Lycée Odilon Redon in Pauillac as part of the “1 Scientifique, 1 Classe, Chiche !” program.
    </div>
    <div class="image">
        <img src="../img/Logo_1-Scientifique_1-Classe_CHICHE-1.png" alt="Chiche2022" />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>October 2022:</b></u> Participation in Inria's Fête de la Science (Circuit Scientifique Bordelais), animation about Computational Neuroscience.
    </div>
    <div class="image">
        <img src="../img/logo_circuit_scientifique_bordelais.png" alt="CircuitBordelais2022" />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>June 2022:</b></u> Visit of the Montessori International School in Gradignan, animation about innovation, creativity and research.
    </div>
    <div class="image">
        <img src="../img/Photo_Montessori.jpg" alt="PhotoMontessori" />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>May 2022:</b></u> Participation in the "Bureau des Enquêtes" animation of Cap Sciences at the Bordeaux GeekFest.
    </div>
    <div class="image">
        <img src="../img/cap_sciences_logo.jpg" alt="CapSciences" />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>2022:</b></u> Supervision of a group of highschool scientific research club in Civray (winners of the Cgénial National Science Contest and representing France in the EUCYS european contest).
    </div>
    <div class="image">
        <img src="../img/cgenial.jpeg" alt="LogoCgenial" />
    </div>
</section>
{{< figure src="../img/ArticleCivray.png" width="200" alt="ElevesCivray" class="center" >}}

----------------------------------------------------------

**2018:** Support for the teaching of Science and Technology in Elementary school (“ASTEP”), in Nancy and Pont-a-Mousson.

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>2017:</b></u> Volonteer facilitator for the exhibition: “From homo numericus to digital citizen” in Nancy.
    </div>
    <div class="image">
        <img src="../img/homo_numericus.png" alt="HomoNumericus" />
    </div>
</section>

----------------------------------------------------------

<section class="spotlightstyle1">
    <div class="content">
        <u><b>2014:</b></u> Organization of educational workshops in chemistry for junior high school children, as part of a project with the “Ose les Sciences !” association.
    </div>
    <div class="image">
        <img src="../img/ose_les_sciences.png" alt="OseLesSciences" />
    </div>
</section>

----------------------------------------------------------
