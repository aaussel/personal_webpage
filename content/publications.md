---
title: Publications
url: "/publications"
image: "img/publications_banner.jpeg"
thumbnail: "img/publis3.jpeg"
---

*To do: automatically get list of publications from HAL*
<?php
  $url = 'https://haltools.archives-ouvertes.fr/Public/afficheRequetePubli.php?auteur_exp=Am%C3%A9lie%2CAussel&idHal=996411&CB_auteur=oui&CB_titre=oui&CB_article=oui&CB_DOI=oui&CB_Resume_court=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css&noHeader';
  $http_page = file_get_contents($url,FILE_USE_INCLUDE_PATH);
  echo $http_page;
?>

**Journal Articles:**
- 2023 (Under review): Vardalakis N., Aussel A., Rougier N.P. and Wagner F.B., A dynamical computational model of theta generation in hippocampal circuits to study theta-gamma oscillations during neurostimulation. eLife. https://www.biorxiv.org/content/10.1101/2023.03.24.534142v1 
- January 2023: Aussel A., Pittman-Polletta B., Fiebelkorn I.C., Kastner S., and Kopell N., Interacting rhythms enhance sensitivity of target detection in a fronto-parietal computational model of visual attention. eLife. https://doi.org/10.7554/eLife.67684
- July 2022: Aussel A., Ranta R., Aron O., Colnat-Coulbois S., Tyvaert L., Maillard L., and Buhry L., Cell to network computational model of the epileptic human hippocampus suggests specific roles of network and channel dysfunctions in ictal and interictal oscillations. J Comput Neurosci. https://link.springer.com/article/10.1007/s10827-022-00829-5 
- October 2018: Aussel A., Buhry L., Tyvaert L and Radu R., A detailed anatomical and mathematical model of the hippocampal formation for the generation of sharp-wave ripples and theta-nested gamma oscillations. J Comput Neurosci 45, 207–221 (2018). https://doi.org/10.1007/s10827-018-0704-x

**PhD manuscript:**

Computational modeling of healthy and epileptic hippocampal oscillations (2019): http://docnum.univ-lorraine.fr/public/DDOC_T_2019_0202_AUSSEL.pdf 

**Peer-reviewed Conference Article:**
- July 2020: Aussel A., Buhry L. and Ranta R., Parameter study of a computational model of the hippocampus through Design of Experiments and Sobol' sensitivity analysis,  2020 42nd Annual International Conference of the IEEE Engineering in Medicine & Biology Society (EMBC 2020).
- December 2017: Stability conditions in ring networks with non-trivial activation functions.  IEEE Conference on Decision and Control, in Melbourne.

**Conference Posters and abstracts (as first author):**
- November 2021: Interacting rhythms enhance sensitivity of target detection in a fronto-parietal computational model of visual attention. Neuroscience 2021 conference of the Society for Neurosciences (Conference Abstract and Poster, online)
- July 2019: Extracellular synaptic and action potential signatures in the hippocampal formation: a modeling study. Organization for Computational Neuroscience CNS 2019 Conference in Barcelona. (Conference Abstract and Poster)
- May 2019: Parameter analysis in a computational model of the hippocampus. NeuroFrance conference in Marseille. (Conference Abstract and Poster).
- December 2018: A computational model of the healthy and epileptic hippocampus over the sleep-wake cycle. GdR NeuralNet in Paris. (Conference Abstract and Oral Presentation) 
- July 2018: A detailed model of the hippocampal formation for the generation of Sharp-Wave Ripples and Theta-nested Gamma oscillations. Organization for Computational Neuroscience CNS 2018 Conference in Seattle. (Conference Abstract and Poster, Student Best Poster Award)
- September 2016: Oscillation Analysis of Ring networks: Application on epileptic hippocampus. Bernstein Conference on computational neuroscience, in Berlin. (Conference Abstract and Poster)


